#!/bin/bash

# This script performs the initialization of the Keptn project and applies all required SLI/SLO/Monitoring configuration
# It assumes to run an a gitlab runner with the following commands available:
# - curl
# - jq
# Supported Keptn version: 0.7.x
# Following variables can be set:
# KEPTN_ENDPOINT: The endpoint URL of Keptn. e.g.: https://keptn.domain.com
# KEPTN_API_TOKEN: The API token to authenticate with the Keptn API. Suggest to set this as a secret.
# KEPTN_PROJECT: The name of the Keptn project. This will be read from the keptn init file. e.g.: helloproject
# KEPTN_SERVICE: The name of the Keptn service. This will be read from the keptn init file. e.g.: helloservice
# KEPTN_STAGE: The name of the Keptn stage. This will be read from the keptn init file. e.g.: staging
# KEPTN_EVAL_MODE: How to define the start and end time of the evaluation. Can be one of the following:
#     - "from_file"(Default): read from files called keptn.test.starttime and keptn.test.endtime that contain only the timestamps. generate in the following format 2021-04-22T15:31:38Z (e.g. using 'date --utc +%FT%TZ')
#     - "custom_range": using the variables KEPTN_EVAL_START and KEPTN_EVAL_END the time ranges can be overwritten
# KEPTN_EVAL_START: when using a custom_range, the start time of the evaluation. generate in the following format 2021-04-22T15:31:38Z (e.g. using 'date --utc +%FT%TZ')
# KEPTN_EVAL_END: when using a custom_range, the end time of the evaluation. generate in the following format 2021-04-22T15:31:38Z (e.g. using 'date --utc +%FT%TZ')
# STOP_ON_FAILURE: Boolean value dictating if the pipeline should fail based on evaluation. 1 by default (fail)

KEPTN_PROJECT=$(cat keptn.init.json|jq -r .project)
KEPTN_SERVICE=$(cat keptn.init.json|jq -r .service)
KEPTN_STAGE=$(cat keptn.init.json|jq -r .stage)
KEPTN_EVAL_START_FILE=keptn.test.starttime
KEPTN_EVAL_END_FILE=keptn.test.endtime
KEPTN_EVAL_MODE_DEFAULT=from_file
STOP_ON_FAILURE_DEFAULT=1
KEPTN_DEBUG_DEFAULT=0
eval_start=""
eval_end=""

# Check if all variables are set

if [ "$KEPTN_DEBUG" = "" ]; then
    echo "DEBUG has not been set, defaulting to $KEPTN_DEBUG_DEFAULT"
    KEPTN_DEBUG="$KEPTN_DEBUG_DEFAULT"
fi

if [ "$KEPTN_API_TOKEN" = "" ]; then
    echo "KEPTN_API_TOKEN has not been set!"
    exit 1
fi

if [ "$KEPTN_ENDPOINT" = "" ]; then
    echo "KEPTN_ENDPOINT has not been set!"
    exit 1
fi

if [ "$KEPTN_PROJECT" = "" ]; then
    echo "KEPTN_PROJECT has not been set!"
    exit 1
fi

if [ "$KEPTN_STAGE" = "" ]; then
    echo "KEPTN_STAGE has not been set!"
    exit 1
fi

if [ "$KEPTN_SERVICE" = "" ]; then
    echo "KEPTN_SERVICE has not been set!"
    exit 1
fi

if [ "$KEPTN_EVAL_MODE" = "" ]; then
    echo "KEPTN_EVAL_MODE has not been set, defaulting to $KEPTN_EVAL_MODE_DEFAULT"
    KEPTN_EVAL_MODE="$KEPTN_EVAL_MODE_DEFAULT"
fi

if [ "$KEPTN_EVAL_MODE" = "from_file" ]; then
    echo "Attempting to read times from $KEPTN_EVAL_START_FILE and $KEPTN_EVAL_END_FILE"
    eval_start=$(cat $KEPTN_EVAL_START_FILE)
    eval_end=$(cat $KEPTN_EVAL_END_FILE)
    if [[ $eval_start = "" || $eval_end = "" ]]
    then
        echo "Unable to read eval_start or eval_end from $KEPTN_EVAL_START_FILE and $KEPTN_EVAL_END_FILE"
        echo "$KEPTN_EVAL_START_FILE contents "
        cat ${KEPTN_EVAL_START_FILE}
        echo "$KEPTN_EVAL_END_FILE contents "
        cat ${KEPTN_EVAL_END_FILE}
        echo "Exiting..."
        exit 1
    fi 
fi


if [ "$KEPTN_EVAL_MODE" = "custom_range" ]; then
    echo "Custom time range specified"
    eval_start=$KEPTN_EVAL_START
    eval_end=$KEPTN_EVAL_END

    if [ "$eval_start" = "" ] || [ "$eval_end" = "" ]; then
        echo "KEPTN_EVAL_START or KEPTN_EVAL_END not set."
        echo "Please set using this format: 2021-04-22T08:28:50.000Z"
        echo "This can be done using this command:     date --utc +%FT%T.000Z"
        echo "Exiting..."
        exit 1
    fi 
fi

if [ "$STOP_ON_FAILURE" = "" ]; then
    echo "STOP_ON_FAILURE has not been set, defaulting to $STOP_ON_FAILURE_DEFAULT"
    STOP_ON_FAILURE="$STOP_ON_FAILURE_DEFAULT"
fi

# Endpoints
KEPTN_API=$KEPTN_ENDPOINT/api
KEPTN_BRIDGE=$KEPTN_ENDPOINT/bridge
KEPTN_ENDPOINT_PROJECT=$KEPTN_API/v1/project
KEPTN_ENDPOINT_SERVICE=$KEPTN_API/v1/project/$KEPTN_PROJECT/service
KEPTN_ENDPOINT_RESOURCE=$KEPTN_API/configuration-service/v1/project/$KEPTN_PROJECT/stage/$KEPTN_STAGE/service/$KEPTN_SERVICE/resource
KEPTN_ENDPOINT_EVENT=$KEPTN_API/v1/event
KEPTN_ENDPOINT_EVALRES=$KEPTN_API/mongodb-datastore/event

echo "Keptn Init Project"
echo "=================="
echo "Keptn API Token:     $KEPTN_API_TOKEN"
echo "Keptn Endpoint:      $KEPTN_ENDPOINT"
echo "Keptn API:           $KEPTN_API"
echo "Keptn Bridge:        $KEPTN_BRIDGE"
echo "Keptn Project:       $KEPTN_PROJECT"
echo "Keptn Stage:         $KEPTN_STAGE"
echo "Keptn Service:       $KEPTN_SERVICE"
echo "Addtl. Keptn Labels: $KEPTN_LABELS"
echo "Evaluation Start:    $eval_start" 
echo "Evaluation End:      $eval_end"
echo "Stop on failure:     $STOP_ON_FAILURE"

LABELS_BODY=""

for k in $(jq '. | keys | .[]' <<< "${KEPTN_LABELS}"); do
    value=$(jq -r ".[$k]" <<< "${KEPTN_LABELS}");
    key=$(jq -r 'keys | .[0]' <<< "$value");
    val=$(jq -r ". | .$key" <<< "$value");
    LABELS_BODY="$LABELS_BODY \"$key\":\"$val\","
done

LABELS_BODY="$LABELS_BODY \"buildId\": \"$CI_PIPELINE_IID\","
LABELS_BODY="$LABELS_BODY \"buildNumber\": \"$CI_PIPELINE_IID\","
LABELS_BODY="$LABELS_BODY \"jobname\": \"$CI_JOB_NAME\","
LABELS_BODY="$LABELS_BODY \"joburl\": \"$CI_JOB_URL\","
LABELS_BODY="$LABELS_BODY \"project\": \"$CI_PROJECT_NAME\""

eval_res=$(curl -k --location --request POST "$KEPTN_ENDPOINT_EVENT" \
--header "x-token: $KEPTN_API_TOKEN" \
--header 'Content-Type: application/json' \
--data-raw "{
    \"datacontenttype\": \"application/json\",
    \"data\": {
        \"project\": \"$KEPTN_PROJECT\",
        \"service\": \"$KEPTN_SERVICE\",
        \"stage\": \"$KEPTN_STAGE\",
        \"teststrategy\": \"manual\",
        \"image\": \"$CI_PROJECT_NAME\",
        \"tag\": \"$CI_PIPELINE_IID\",
        \"labels\": {
            $LABELS_BODY
        },
        \"test\": {
            \"start\": \"$eval_start\",
            \"end\": \"$eval_end\"
        },
        \"evaluation\": {
            \"start\": \"$eval_start\",
            \"end\": \"$eval_end\"
        }
    },
    \"source\": \"gitlab-library\",
    \"specversion\": \"1.0\",
    \"type\": \"sh.keptn.event.$KEPTN_STAGE.evaluation.triggered\"
}")

#echo -e "Evaluation Result: $eval_res"
keptn_context=$(echo $eval_res | jq -r .keptnContext)
echo -e "KeptnContext:      $keptn_context"
# Poll the keptn API every 10 seconds with the context ID for a evaluation-done event
fin="0" 
until [ "$fin" = "1" ]
do
    cnt=$((cnt + 1))
    event_result=$(curl -s -k -X GET "$KEPTN_ENDPOINT_EVALRES?keptnContext=$keptn_context&type=sh.keptn.event.evaluation.finished" -H "accept: application/json" -H "x-token: ${KEPTN_API_TOKEN}")
    
    if [ "$DEBUG" = "1" ]; then
        echo "Response from API: "
        echo "$event_result"
    fi

    if [[ "$event_result" == *"No Keptn sh.keptn.event.evaluation.finished event found for context"* || "$event_result" == *"{\"events\":[],"* ]]; then
        echo "Evaluation result not yet available, retrying in 10s"
        sleep 10
    else
        status=$(echo $event_result|jq .events[0].data.evaluation.result)
        score=$(echo $event_result|jq .events[0].data.evaluation.score)
        fin="1"
    fi
    if [ "$cnt" = "30" ]; then
        echo "Keptn timed out evaluating your deployment!"
        exit 1
    fi
done

if [ "$DEBUG" = "1" ]; then
    echo "Evaluation result raw: "
    echo $event_result | jq .
fi

# Save result as artifact
echo $event_result > keptn.evaluation-done.json

# Get the Event ID from the evaluation-done result
#eventid=$(echo $event_result|jq -r .id)

# Build keptn bridge deep link to the evaluation    
bridge_url="${KEPTN_BRIDGE}/trace/${keptn_context}"

# Let the Pipeline fail when the evaluation failed. 

if [ "$status" = "\"fail\"" ]; then
    echo "Keptn Quality Gate - Evaluation failed! - Score $score"
    echo "For details visit the Bridge!"
    echo $bridge_url

    if [[ $STOP_ON_FAILURE="1" ]]; then
    echo "STOP_ON_FAILURE set to 1, failing job"
    exit 1
    fi   
    
else
    echo "Evaluation finished."
    echo "For details visit the Bridge!"
    echo $bridge_url
fi