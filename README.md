# Deprecation notice

**This repository has been deprecated. [Dynatrace Cloud Automation](https://www.dynatrace.com/platform/cloud-automation/) as well as [Keptn](https://keptn.sh) Quality Gate CI/CD integrations were combined in the new [cloud-automation-tools](https://github.com/dynatrace-ace/cloud-automation-tools) repository.**

---

# Keptn GitLab Runner

Container image containing logic to integrate quality gates usecases with your GitLab CI Pipelines

| Author | Container Image Tag | Keptn Supported Version | Comment |
| ----- | --------------- | ------- | ------ |
| [@kristofre](https://gitlab.com/kristofre)| 1.0 | 0.7.x | Initial Release |
| [@kristofre](https://gitlab.com/kristofre)| 2.0 | 0.8.3 | Support for Keptn 0.8.3 |
| [@tobigremmer-dt](https://gitlab.com/tobigremmer-dt)| 2.1 | 0.8.3 | Support for environment variables containing custom timezones |

## Stages

> Note: there is a full sample pipeline in the samples directory
### Keptn Init
Keptn needs to be initialized (project, stage, service, SLOs, SLIs, monitoring, ...) to inform Keptn about what is going to be evaluated.

You can add the stage to the `.gitlab-ci.yaml` file:

```yaml
prepare_keptn:
  image: dynatraceace/keptn-gitlab-runner:latest
  stage: init
  environment:
    name: test
  variables: 
    KEPTN_PROJECT: simplenode-gitlab
    KEPTN_SERVICE: simplenodeservice
    KEPTN_STAGE: staging
    KEPTN_SOURCE: gitlab
    KEPTN_MONITORING: dynatrace
  script:
    - /keptn/keptn_init.sh
  artifacts:
    paths: 
    - keptn.init.json
```

The following variables can be set, either on the job, pipeline or global level:
| Variable  | Description | Required | Default |
| --------- | ----------- | -------- | ------- |
| KEPTN_ENDPOINT | The endpoint URL of Keptn. e.g.: https://keptn.domain.com | **yes** | |
| KEPTN_API_TOKEN | The API token to authenticate with the Keptn API. Suggest to set this as a secret. | **yes** | |
| KEPTN_PROJECT | The name of the Keptn project. e.g.: helloproject | **yes** | |
| KEPTN_SERVICE | The name of the Keptn service. e.g.: helloservice | **yes** | |
| KEPTN_STAGE | The name of the Keptn stage. e.g.: staging | **yes** | |
| KEPTN_MONITORING | The solution used for monitoring | no | dynatrace |
| SHIPYARD_FILE | The path location of the shipyard file | no | keptn/shipyard.yaml |
| SLO_FILE | The path location of the SLO file | no | keptn/slo.yaml |
| SLI_FILE | The path location of the SLI file | no | keptn/sli.yaml |
| DT_CONFIG_FILE | The path location of the Dynatrace conf file | no | keptn/dynatrace.conf.yaml |
| KEPTN_DEBUG | Debug output | no | 0 |

> Note: During Keptn init, a file called `keptn.init.${CI_PIPELINE_IID}.json` gets created that contains the following data and that can be reused in future stages:
- KEPTN_PROJECT
- KEPTN_SERVICE
- KEPTN_STAGE
- KEPTN_ENDPOINT
- KEPTN_BRIDGE_ENDPOINT
### Keptn Eval
Once the tests have been run, this job needs to execute to evaluate the SLOs that were defined.

You can add the stage to the `.gitlab-ci.yaml` file:

```yaml
quality_gate:
  image: dynatraceace/keptn-gitlab-runner:latest
  stage: eval
  environment:
    name: test
  variables: 
    KEPTN_LABELS: '[{"label1":"foo"},{"label2":"bar"}]'
  script:
    - /keptn/keptn_eval.sh
```

The following variables can be set, either on the job, pipeline or global level:
| Variable  | Description | Required | Default |
| --------- | ----------- | -------- | ------- |
| KEPTN_ENDPOINT | The endpoint URL of Keptn. e.g.: https://keptn.domain.com. This would normally already be set in the keptn_init job | **yes** | |
| KEPTN_API_TOKEN | The API token to authenticate with the Keptn API. Suggest to set this as a secret. This would normally already be set in the keptn_init job| **yes** | |
| KEPTN_PROJECT | The name of the Keptn project. e.g.: helloproject | **yes** | read from `keptn.init.${CI_PIPELINE_IID}.json` |
| KEPTN_SERVICE | The name of the Keptn service. e.g.: helloservice | **yes** | read from `keptn.init.${CI_PIPELINE_IID}.json` |
| KEPTN_STAGE | The name of the Keptn stage. e.g.: staging | **yes** | read from `keptn.init.${CI_PIPELINE_IID}.json` |
| KEPTN_EVAL_MODE | How to define the start and end time of the evaluation. Can be one of the following: `from_file` or `custom_range`. See section below on setting timestamps | no | from_file |
| KEPTN_EVAL_START | When using `custom_range` for KEPTN_EVAL_MODE, the start time of the evaluation. See section below on setting timestamps | no | |
| KEPTN_EVAL_END | When using `custom_range` for KEPTN_EVAL_MODE, the end time of the evaluation. See section below on setting timestamps | no | |
| STOP_ON_FAILURE | Boolean value dictating if the pipeline should fail based on evaluation | no | 1 |
| KEPTN_LABELS | Additional labels that are sent to Keptn when evaluation. They need to be set in the following format: `'[{"someLabel":"foo"},{"anotherLabel":"bar"}]'` | no | |


#### Setting time stamps for evaluation
Evaluation times can be define in one of two ways

1. Using artefact files
   When using this way, in your test execution, ensure two artifact files get created that contain the time stamps of your test start and end.

   The files that are to be created are `keptn.test.starttime` and `keptn.test.endtime` and need to contain the UTC date in the following format: `2021-04-22T15:31:38Z`. They can be generated using `date --utc +%FT%TZ`. In the sample `.gitlab-ci.yaml` file you can find an example using `before_script` and `after_script` blocks. In the `artifacts` block they will be added for futher use in the next job. 

    ```yaml
    run-tests:
      extends: .run-jmeter
      before_script: 
        - echo $(date --utc +%FT%TZ) > keptn.test.starttime
      after_script: 
        - echo $(date --utc +%FT%TZ) > keptn.test.endtime
      stage: test   
      environment:
        name: test
      artifacts:
        paths:
        - keptn.test.starttime
        - keptn.test.endtime
    ```

    Set the variable `KEPTN_EVAL_MODE` to `from_file`, which is also the default.

2. Setting variables on the stage
   By setting the variable `KEPTN_EVAL_MODE` to `custom_range`, you can use the additional variables `KEPTN_EVAL_START` and `KEPTN_EVAL_END` to set start and end times in UTC date in the following format: `2021-04-22T15:31:38Z`. They can be generated using `date --utc +%FT%TZ`
   
#### Setting time stamps for evaluation with custom time zone

All variables including time information and expanded at runtime will by default use UTC. A different timezone can be set by adding *TZ=\<[timezone abbreviation](https://www.timeanddate.com/time/zones/) - OR - [name in tz database](https://www.iana.org/time-zones)\>* before *date* is called. For example:

  ```yaml
  run-tests:
    extends: .run-jmeter
    before_script: 
      - echo $(TZ=America/New_York date +%FT%TZ) > keptn.test.starttime
    after_script: 
      - echo $(TZ=America/New_York date +%FT%TZ) > keptn.test.endtime
    stage: test   
    environment:
      name: test
    artifacts:
      paths:
      - keptn.test.starttime
      - keptn.test.endtime
  ```
