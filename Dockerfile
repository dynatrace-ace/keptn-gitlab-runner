FROM alpine:latest
LABEL version="v0.0.1" maintainer="Dynatrace ACE team<ace@dynatrace.com>"

ARG KEPTN_VERSION="0.8.3"
ARG YQ_VERSION="4.7.1"
ARG MONACO_VERSION="v1.5.3"

RUN apk add --update --no-cache \
    curl \
    jq \
    ca-certificates \
    bash \
    nss \
    unzip \
    util-linux \
    wget \
    libc6-compat \
    coreutils \
    tzdata

# Install Keptn CLI
#RUN curl -LO https://github.com/keptn/keptn/releases/download/${KEPTN_VERSION}/keptn-${KEPTN_VERSION}-linux-amd64.tar.gz && \
#    tar -xvf ${KEPTN_VERSION}_keptn-linux.tar && chmod +x keptn-${KEPTN_VERSION}-linux-amd64 && mv keptn-${KEPTN_VERSION}-linux-amd64 /usr/bin/keptn 

# Install YQ
RUN curl -sL https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_linux_amd64 -o /usr/bin/yq
RUN chmod +x /usr/bin/yq

RUN mkdir /keptn
ADD keptn_eval.sh /keptn/keptn_eval.sh
ADD keptn_init.sh /keptn/keptn_init.sh
RUN chmod +x /keptn/keptn_eval.sh
RUN chmod +x /keptn/keptn_init.sh

ENTRYPOINT ["/bin/bash", "-l", "-c"]
