#!/bin/bash

# This script performs the initialization of the Keptn project and applies all required SLI/SLO/Monitoring configuration
# It assumes to run an a gitlab runner with the following commands available:
# - curl
# - jq
# Supported Keptn version: 0.7.x
# Following variables can be set:
# KEPTN_ENDPOINT: The endpoint URL of Keptn. e.g.: https://keptn.domain.com
# KEPTN_API_TOKEN: The API token to authenticate with the Keptn API. Suggest to set this as a secret.
# KEPTN_PROJECT: The name of the Keptn project. e.g.: helloproject
# KEPTN_SERVICE: The name of the Keptn service. e.g.: helloservice
# KEPTN_STAGE: The name of the Keptn stage. e.g.: staging
# KEPTN_MONITORING: The solution used for monitoring. This defaults to "dynatrace"
# SHIPYARD_FILE: this location of the shipyard file. Defaults to "keptn/shipyard.yaml"
# SLO_FILE: the location of the SLO file.  Defaults to "keptn/slo.yaml"
# SLI_FILE: the location of the SLI file.  Defaults to "keptn/sli.yaml"
# DT_CONFIG_FILE: the location of the Dynatrace config file.  Defaults to "keptn/dynatrace.conf.yaml"
# KEPTN_SOURCE: Source system creating the Keptn config. Defaults to "GitLab"

KEPTN_MONITORING_DEFAULT=dynatrace
SHIPYARD_FILE_DEFAULT=keptn/shipyard.yaml
SLO_FILE_DEFAULT=keptn/slo.yaml
SLI_FILE_DEFAULT=keptn/sli.yaml
DT_CONFIG_FILE_DEFAULT=keptn/dynatrace.conf.yaml
KEPTN_SOURCE_DEFAULT=GitLab
KEPTN_DEBUG_DEFAULT=0

# Check if all variables are set

if [ "$KEPTN_DEBUG" = "" ]; then
    echo "DEBUG has not been set, defaulting to $KEPTN_DEBUG_DEFAULT"
    KEPTN_DEBUG="$KEPTN_DEBUG_DEFAULT"
fi

if [ "$KEPTN_API_TOKEN" = "" ]; then
    echo "KEPTN_API_TOKEN has not been set!"
    exit 1
fi

if [ "$KEPTN_ENDPOINT" = "" ]; then
    echo "KEPTN_ENDPOINT has not been set!"
    exit 1
fi

if [ "$KEPTN_PROJECT" = "" ]; then
    echo "KEPTN_PROJECT has not been set!"
    exit 1
fi

if [ "$KEPTN_STAGE" = "" ]; then
    echo "KEPTN_STAGE has not been set!"
    exit 1
fi

if [ "$KEPTN_SERVICE" = "" ]; then
    echo "KEPTN_SERVICE has not been set!"
    exit 1
fi

if [ "$KEPTN_MONITORING" = "" ]; then
    echo "KEPTN_MONITORING has not been set, defaulting to $KEPTN_MONITORING_DEFAULT"
    KEPTN_MONITORING=$KEPTN_MONITORING_DEFAULT
fi

if [ "$KEPTN_SOURCE" = "" ]; then
    echo "KEPTN_SOURCE has not been set, defaulting to $KEPTN_SOURCE_DEFAULT"
    KEPTN_SOURCE=$KEPTN_SOURCE_DEFAULT
fi

KEPTN_API=$KEPTN_ENDPOINT/api
KEPTN_BRIDGE=$KEPTN_ENDPOINT/bridge

echo "Keptn Init Project"
echo "=================="
echo "Keptn API Token: $KEPTN_API_TOKEN"
echo "Keptn Endpoint: $KEPTN_ENDPOINT"
echo "Keptn API: $KEPTN_API"
echo "Keptn Bridge: $KEPTN_BRIDGE"
echo "Keptn Project: $KEPTN_PROJECT"
echo "Keptn Stage: $KEPTN_STAGE"
echo "Keptn Service: $KEPTN_SERVICE"
echo "Keptn Monitoring: $KEPTN_MONITORING"

# Endpoints
KEPTN_ENDPOINT_PROJECT=$KEPTN_ENDPOINT/api/controlPlane/v1/project
KEPTN_ENDPOINT_SERVICE=$KEPTN_ENDPOINT_PROJECT/$KEPTN_PROJECT/service
KEPTN_ENDPOINT_RESOURCE=$KEPTN_ENDPOINT/api/configuration-service/v1/project/$KEPTN_PROJECT/stage/$KEPTN_STAGE/service/$KEPTN_SERVICE/resource
KEPTN_ENDPOINT_EVENT=$KEPTN_ENDPOINT/api/v1/event

keptn_init_json_file="keptn.init.json"
keptn_init_json_contents="{\"project\": \"$KEPTN_PROJECT\", \"service\": \"$KEPTN_SERVICE\", \"stage\": \"$KEPTN_STAGE\", \"keptn_endpoint\": \"$KEPTN_API\", \"keptn_bridge\": \"$KEPTN_BRIDGE\"}"
echo $keptn_init_json_contents > $keptn_init_json_file


if [ "$SHIPYARD_FILE" = "" ]; then
    echo "SHIPYARD_FILE has not been set, defaulting to $SHIPYARD_FILE_DEFAULT"
    SHIPYARD_FILE=$SHIPYARD_FILE_DEFAULT
fi

echo -e "Create project $KEPTN_PROJECT"
shipyardb64=$(base64 ./$SHIPYARD_FILE | tr -d \\n)

curl -s -k --location --request POST "$KEPTN_ENDPOINT_PROJECT" \
--header "x-token: $KEPTN_API_TOKEN" \
--header 'Content-Type: application/json' \
--data-raw "{
    \"name\": \"$KEPTN_PROJECT\",
    \"shipyard\": \"$shipyardb64\"
}"

sleep 10

echo -e "Create Service $KEPTN_SERVICE"
curl -s -k --location --request POST "$KEPTN_ENDPOINT_SERVICE" \
--header "x-token: $KEPTN_API_TOKEN" \
--header 'Content-Type: application/json' \
--data-raw "{
    \"serviceName\": \"$KEPTN_SERVICE\"
}"

if [ "$SLO_FILE" = "" ]; then
    echo "SLO_FILE has not been set, defaulting to $SLO_FILE_DEFAULT"
    SLO_FILE=$SLO_FILE_DEFAULT
fi

echo -e "Upload SLO"
slob64=$(base64 ./$SLO_FILE | tr -d \\n)

KEPTN_RESOURCE_URL="$KEPTN_ENDPOINT/api/configuration-service/v1/project/$KEPTN_PROJECT/stage/$KEPTN_STAGE/service/$KEPTN_SERVICE/resource"

curl -s -k --location --request POST "$KEPTN_ENDPOINT_RESOURCE" \
--header "x-token: $KEPTN_API_TOKEN" \
--header 'Content-Type: application/json' \
--data-raw "{
    \"resources\" : [
    {
        \"resourceURI\": \"slo.yaml\",
        \"resourceContent\": \"$slob64\"
    }
    ]
}"

if [ "$SLI_FILE" = "" ]; then
    echo "SLI_FILE has not been set, defaulting to $SLI_FILE_DEFAULT"
    SLI_FILE=$SLI_FILE_DEFAULT
fi

echo -e "Upload SLI"
slib64=$(base64 ./$SLI_FILE | tr -d \\n)

curl -s -k --location --request POST "$KEPTN_ENDPOINT_RESOURCE" \
--header "x-token: $KEPTN_API_TOKEN" \
--header 'Content-Type: application/json' \
--data-raw "{
    \"resources\" : [
    {
        \"resourceURI\": \"dynatrace/sli.yaml\",
        \"resourceContent\": \"$slib64\"
    }
    ]
}"

if [ "$DT_CONFIG_FILE" = "" ]; then
    echo "DT_CONFIG_FILE has not been set, defaulting to $DT_CONFIG_FILE_DEFAULT"
    DT_CONFIG_FILE=$DT_CONFIG_FILE_DEFAULT
fi

echo -e "Upload DT Config"
conf64=$(base64 ./$DT_CONFIG_FILE | tr -d \\n)

curl -s -k --location --request POST "$KEPTN_ENDPOINT_RESOURCE" \
--header "x-token: $KEPTN_API_TOKEN" \
--header 'Content-Type: application/json' \
--data-raw "{
    \"resources\" : [
    {
        \"resourceURI\": \"dynatrace/dynatrace.conf.yaml\",
        \"resourceContent\": \"$conf64\"
    }
    ]
}"


echo -e "Configure Monitoring $KEPTN_MONITORING"

curl -s -k --location --request POST "$KEPTN_ENDPOINT_EVENT" \
--header "x-token: $KEPTN_API_TOKEN" \
--header 'Content-Type: application/json' \
--data-raw "{
    \"contenttype\": \"application/json\",
    \"data\": {
    \"project\": \"$KEPTN_PROJECT\",
    \"service\": \"$KEPTN_SERVICE\",
    \"type\": \"$KEPTN_MONITORING\"
    },
    \"source\": \"$KEPTN_SOURCE\",
    \"specversion\": \"0.2\",
    \"type\": \"sh.keptn.event.monitoring.configure\"
}"